package com.example.vendingmachine.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 冼栋梁 on 2018/3/24.
 */

public class TestData {

    private List<String> http_url = new ArrayList<>();
    private List<String> price = new ArrayList<>();
    private List<String> name = new ArrayList<>();
    private List<Integer> ID = new ArrayList<>();
    private List<Item> Item_List = new ArrayList<>();

    private void add_url(){

        http_url.add(0,"http://img4.imgtn.bdimg.com/it/u=1547209661,2856987165&fm=27&gp=0.jpg");
        http_url.add(1,"http://img2.imgtn.bdimg.com/it/u=2049844046,3440087944&fm=27&gp=0.jpg");
        http_url.add(2,"http://img5.imgtn.bdimg.com/it/u=1361715243,1280790974&fm=27&gp=0.jpg");
        http_url.add(3,"http://img1.imgtn.bdimg.com/it/u=1290033591,704151454&fm=27&gp=0.jpg");
        http_url.add(4,"http://img3.imgtn.bdimg.com/it/u=1419230222,1978063963&fm=27&gp=0.jpg");
        http_url.add(5,"http://img4.imgtn.bdimg.com/it/u=793325144,4074104850&fm=27&gp=0.jpg");
        http_url.add(6,"http://img5.imgtn.bdimg.com/it/u=1177704801,269421278&fm=27&gp=0.jpg");
        http_url.add(7,"https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=1462575793,522490717&fm=27&gp=0.jpg");
        http_url.add(8,"http://img2.imgtn.bdimg.com/it/u=1809329263,2941505063&fm=27&gp=0.jpg");
        http_url.add(9,"http://img1.imgtn.bdimg.com/it/u=989201677,4179447844&fm=27&gp=0.jpg");
        http_url.add(10,"http://img1.imgtn.bdimg.com/it/u=2066016988,4279809013&fm=27&gp=0.jpg");
        http_url.add(11,"http://img1.imgtn.bdimg.com/it/u=2325893737,1652481246&fm=27&gp=0.jpg");
        http_url.add(12,"http://img2.imgtn.bdimg.com/it/u=2631300946,1027330168&fm=27&gp=0.jpg");
        http_url.add(13,"http://img4.imgtn.bdimg.com/it/u=793325144,4074104850&fm=27&gp=0.jpg");
        http_url.add(14,"http://img4.imgtn.bdimg.com/it/u=4106542085,3447574179&fm=27&gp=0.jpg");
        http_url.add(15,"http://img4.imgtn.bdimg.com/it/u=1685783547,3385040993&fm=27&gp=0.jpg");
        http_url.add(16,"http://img4.imgtn.bdimg.com/it/u=2627651380,793831032&fm=27&gp=0.jpg");
        http_url.add(17,"http://img2.imgtn.bdimg.com/it/u=3855444162,1766749062&fm=27&gp=0.jpg");
        http_url.add(18,"http://img3.imgtn.bdimg.com/it/u=1106288764,3243917823&fm=27&gp=0.jpg");
        http_url.add(19,"http://img0.imgtn.bdimg.com/it/u=3280462065,32481051&fm=27&gp=0.jpg");
        http_url.add(20,"http://img3.imgtn.bdimg.com/it/u=4145760796,4160083755&fm=27&gp=0.jpg");
        http_url.add(21,"http://img3.imgtn.bdimg.com/it/u=1566589517,323864302&fm=11&gp=0.jpg");
        http_url.add(22,"http://img4.imgtn.bdimg.com/it/u=2808428382,4279181444&fm=27&gp=0.jpg");
        http_url.add(23,"http://img2.imgtn.bdimg.com/it/u=971334253,713518903&fm=27&gp=0.jpg");
        http_url.add(24,"http://img5.imgtn.bdimg.com/it/u=3351390160,487017169&fm=27&gp=0.jpg");
        http_url.add(25,"http://img0.imgtn.bdimg.com/it/u=346392699,1428322029&fm=27&gp=0.jpg");
        http_url.add(26,"http://img2.imgtn.bdimg.com/it/u=2008229257,2253480289&fm=27&gp=0.jpg");
        http_url.add(27,"http://img4.imgtn.bdimg.com/it/u=2500621581,107984983&fm=27&gp=0.jpg");
        http_url.add(28,"http://img1.imgtn.bdimg.com/it/u=2793466433,48608684&fm=27&gp=0.jpg");
        http_url.add(29,"http://img3.imgtn.bdimg.com/it/u=4234202148,1240134739&fm=27&gp=0.jpg");

    }

    private void add_price(){

        price.add(0,"￥1.00");
        price.add(1,"￥2.00");
        price.add(2,"￥3.00");
        price.add(3,"￥4.00");
        price.add(4,"￥5.00");
        price.add(5,"￥6.00");
        price.add(6,"￥7.00");
        price.add(7,"￥8.00");
        price.add(8,"￥9.00");
        price.add(9,"￥10.00");
        price.add(10,"￥11.00");
        price.add(11,"￥12.00");
        price.add(12,"￥13.00");
        price.add(13,"￥14.00");
        price.add(14,"￥15.00");
        price.add(15,"￥16.00");
        price.add(16,"￥17.00");
        price.add(17,"￥18.00");
        price.add(18,"￥19.00");
        price.add(19,"￥20.00");
        price.add(20,"￥21.00");
        price.add(21,"￥22.00");
        price.add(22,"￥23.00");
        price.add(23,"￥24.00");
        price.add(24,"￥25.00");
        price.add(25,"￥26.00");
        price.add(26,"￥27.00");
        price.add(27,"￥28.00");
        price.add(28,"￥29.00");
        price.add(29,"￥30.00");

    }

    private void add_name(){

        name.add(0,"可口可乐");
        name.add(1,"雪碧");
        name.add(2,"薯片");
        name.add(3,"阿萨姆奶茶");
        name.add(4,"特仑苏");
        name.add(5,"伊利");
        name.add(6,"蒙牛");
        name.add(7,"金典");
        name.add(8,"子母奶");
        name.add(9,"雷蛇");
        name.add(10,"美盗商船");
        name.add(11,"酷冷至尊");
        name.add(12,"樱桃");
        name.add(13,"杜蕾斯");
        name.add(14,"苹果牛奶");
        name.add(15,"草莓牛奶");
        name.add(16,"香蕉牛奶");
        name.add(17,"咖啡");
        name.add(18,"百事可乐");
        name.add(19,"波板糖");
        name.add(20,"口香糖");
        name.add(21,"彩虹糖");
        name.add(22,"曲奇");
        name.add(23,"巧克力");
        name.add(24,"酱香饼");
        name.add(25,"老干妈");
        name.add(26,"卫龙辣条");
        name.add(27,"营养快线");
        name.add(28,"汉堡包");
        name.add(29,"三明治");

    }

    private void add_ID(){

        ID.add(0,1);
        ID.add(1,2);
        ID.add(2,3);
        ID.add(3,4);
        ID.add(4,5);
        ID.add(5,6);
        ID.add(6,7);
        ID.add(7,8);
        ID.add(8,9);
        ID.add(9,10);
        ID.add(10,11);
        ID.add(11,12);
        ID.add(12,13);
        ID.add(13,14);
        ID.add(14,15);
        ID.add(15,16);
        ID.add(16,17);
        ID.add(17,18);
        ID.add(18,19);
        ID.add(19,20);
        ID.add(20,21);
        ID.add(21,22);
        ID.add(22,23);
        ID.add(23,24);
        ID.add(24,25);
        ID.add(25,26);
        ID.add(26,27);
        ID.add(27,28);
        ID.add(28,29);
        ID.add(29,30);

    }

    public List<Item> create_Item(){

        add_url();

        add_price();

        add_name();

        add_ID();

        for (int i = 0 ; i < ID.size() ; i++){

            Item item = new Item(ID.get(i),name.get(i),price.get(i),http_url.get(i));

            Item_List.add(i,item);

        }


        return Item_List;
    }

}
