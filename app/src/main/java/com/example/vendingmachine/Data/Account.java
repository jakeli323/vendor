package com.example.vendingmachine.Data;

public class Account {

    private String accountNumber;
    private String accountPassword;

    public Account(String accountNumber,String accountPassword){

        this.accountNumber = accountNumber;
        this.accountPassword = accountPassword;
    }

    public String getAccountNumber() {
        return accountNumber;
    }
    public String getAccountPassword() {
        return accountPassword;
    }

}
