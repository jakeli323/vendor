package com.example.vendingmachine.Data;

/**
 * Created by 冼栋梁 on 2018/3/18.
 */

public class Item {

    private int ID = 0;
    private String Price;
    private String Name = "Commodity_Name";
    private String ImagePath;

    public Item(int ID, String Name, String Price, String ImagePath){

        this.ID = ID;
        this.Name = Name;
        this.Price = Price;
        this.ImagePath = ImagePath;

    }

    public int getID(){
        return ID;
    }
    public String getPrice(){
        return Price;
    }
    public String getName(){
        return Name;
    }
    public String getImagePath(){
        return ImagePath;
    }

}
