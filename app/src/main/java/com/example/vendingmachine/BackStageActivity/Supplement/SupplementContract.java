package com.example.vendingmachine.BackStageActivity.Supplement;

import android.app.FragmentManager;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;

/**
 * Created by 冼栋梁 on 2018/4/9.
 */

public interface SupplementContract {

    void getSupplementView(@NonNull View view);

    void getSupplementContext(@NonNull Context context);

    void getSupplementManager(@NonNull FragmentManager supplementManager);

    void setClickFinishBackStage(ClickFinishBackStage clickFinishBackStage);

    interface ClickFinishBackStage{

        void finishBackStage();
    }
}
