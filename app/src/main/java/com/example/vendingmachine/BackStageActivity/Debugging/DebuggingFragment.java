package com.example.vendingmachine.BackStageActivity.Debugging;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.vendingmachine.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class DebuggingFragment extends Fragment{

    public DebuggingFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.backstage_debugging_fragment, container, false);

        return view;
    }

}
