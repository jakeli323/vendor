package com.example.vendingmachine.BackStageActivity;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.widget.RadioGroup;

import com.example.vendingmachine.BackStageActivity.BaudRate.BaudRateFragment;
import com.example.vendingmachine.BackStageActivity.Debugging.DebuggingFragment;
import com.example.vendingmachine.BackStageActivity.Supplement.SupplementFragment;
import com.example.vendingmachine.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 冼栋梁 on 2018/4/9.
 */

public class BackStagePresenter implements BackStageContract,RadioGroup.OnCheckedChangeListener{

    private FragmentManager backstage_fragmentManager;

    private List<Fragment> fragmentList;

    @Override
    public void getBackStageManager(FragmentManager backstage_fragmentManager){

        this.backstage_fragmentManager = backstage_fragmentManager;
    }

    @Override
    public void getNavigationGroup(RadioGroup backstage_navigationGroup){

        fragmentList = initFragments();
        backstage_navigationGroup.check(R.id.radio_btn_backstage_supplement_pressed);
        switchFragment(0);
        backstage_navigationGroup.setOnCheckedChangeListener(this);

    }

    private List<Fragment> initFragments(){

        List<Fragment> fragmentList = new ArrayList<>();

        SupplementFragment supplementFragment = new SupplementFragment();
        fragmentList.add(0,supplementFragment);
        BaudRateFragment baudRateFragment = new BaudRateFragment();
        fragmentList.add(1,baudRateFragment);
        DebuggingFragment debuggingFragment = new DebuggingFragment();
        fragmentList.add(2,debuggingFragment);

        return fragmentList;
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId){

        switch (checkedId){

            case R.id.radio_btn_backstage_supplement_pressed:switchFragment(0);break;
            case R.id.radio_btn_backstage_baudRate_pressed:switchFragment(1);break;
            case R.id.radio_btn_backstage_debugging_pressed:switchFragment(2);break;
        }

    }

    private void switchFragment(int index){

        final FragmentTransaction fragmentTransaction = backstage_fragmentManager.beginTransaction();

        final int size = fragmentList.size();

        for (int i = 0; i < size; i++) {

            Fragment fragment = fragmentList.get(i);

            if (i==index){

                if (fragment.isAdded()){

                    fragmentTransaction.show(fragment);

                }else{

                    fragmentTransaction.add(R.id.rl_backstage_changefragment_change,fragment);

                }

            }else{

                if (fragment.isAdded()){

                    fragmentTransaction.hide(fragment);

                }
            }
        }

        fragmentTransaction.commit();
    }

}
