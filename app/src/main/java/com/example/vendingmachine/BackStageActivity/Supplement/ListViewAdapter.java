package com.example.vendingmachine.BackStageActivity.Supplement;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.vendingmachine.R;

import java.util.List;

public class ListViewAdapter extends BaseAdapter {

    private List<String> nameList;

    private Context supplementContext;

    public ListViewAdapter(Context supplementContext, List<String> nameList){

        this.supplementContext = supplementContext;
        this.nameList = nameList;
    }

    @Override
    public int getCount() {

        int size = nameList.size();

        return size;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;

        if (convertView==null){

            LayoutInflater inflater = (LayoutInflater)supplementContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.backstage_supplement_listview_name,parent,false);
            viewHolder = new ViewHolder();
            viewHolder.tv_backstage_supplement_name_change = convertView.findViewById(R.id.tv_backstage_supplement_listview_name_change);
            convertView.setTag(viewHolder);

        }else {

            viewHolder = (ViewHolder)convertView.getTag();
        }

        viewHolder.tv_backstage_supplement_name_change.setText(nameList.get(position));

        return convertView;
    }

    class ViewHolder{

        public TextView tv_backstage_supplement_name_change;

    }

}
