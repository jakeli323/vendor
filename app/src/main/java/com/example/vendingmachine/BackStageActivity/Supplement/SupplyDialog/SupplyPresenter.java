package com.example.vendingmachine.BackStageActivity.Supplement.SupplyDialog;

import android.content.Context;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.vendingmachine.R;

/**
 * Created by 冼栋梁 on 2018/4/10.
 */

public class SupplyPresenter implements SupplyContract,View.OnClickListener {

    private Context context;
    private ClickCancleSupply clickCancleSupply;
    private ClickConfirmSupply clickConfirmSupply;
    private EditText et_backstage_supplement_number_input;

    public SupplyPresenter(){

    }

    @Override
    public void setClickCancleSupply(ClickCancleSupply clickCancleSupply){

        this.clickCancleSupply = clickCancleSupply;
    }

    @Override
    public void setClickConfirmSupply(ClickConfirmSupply clickConfirmSupply){

        this.clickConfirmSupply = clickConfirmSupply;
    }

    @Override
    public void getSupplyContext(Context context){

        this.context = context;
    }

    @Override
    public void getSupplyView(View view){

        final Button btn_backstage_supplement_supply_back_pressed = view.findViewById(R.id.btn_backstage_supplement_supply_back_pressed);
        btn_backstage_supplement_supply_back_pressed.setOnClickListener(this);

        final Button btn_backstage_supplement_supply_confirm_pressed = view.findViewById(R.id.btn_backstage_supplement_supply_confirm_pressed);
        btn_backstage_supplement_supply_confirm_pressed.setOnClickListener(this);

        et_backstage_supplement_number_input = view.findViewById(R.id.et_backstage_supplement_number_input);
        et_backstage_supplement_number_input.setInputType(InputType.TYPE_CLASS_NUMBER);
        et_backstage_supplement_number_input.setGravity(Gravity.CENTER);

    }

    @Override
    public void onClick(View v){

        switch (v.getId()){

            case R.id.btn_backstage_supplement_supply_back_pressed:clickCancleSupply.finishSupply();break;
            case R.id.btn_backstage_supplement_supply_confirm_pressed:{

                getEditTextInput(et_backstage_supplement_number_input);
                clickConfirmSupply.finishSupply();

            }break;

        }

    }

    private void getEditTextInput(EditText input){

        String number_input = input.getText().toString();
        final int currentNumber = getCurrentNumber();

        compareCurrentAndTotal(number_input,currentNumber);

    }

    private int getCurrentNumber(){

        final int currentNumber = 100;
        return currentNumber;
    }

    private void compareCurrentAndTotal(String number_input,int currentNumber){

        if (number_input.isEmpty()){

            final String null_message = "输入数据不能为空，请重新输入";
            toast(null_message);

        }else {

            final int totalNumber = Integer.parseInt(number_input);

            if (totalNumber >= currentNumber){

                final String sucess_message = "输入数据正确，正在上传中";
                toast(sucess_message);

            }else {

                final String error_message = "输入数据错误，请重新输入";
                toast(error_message);
            }

        }

    }

    private void toast(String message){

        final Toast error_toast = Toast.makeText(context,message,Toast.LENGTH_SHORT);
        error_toast.setGravity(Gravity.CENTER,0,0);
        error_toast.show();

    }

}
