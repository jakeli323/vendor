package com.example.vendingmachine.BackStageActivity.Supplement;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.vendingmachine.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SupplementFragment extends Fragment {

    public SupplementFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.backstage_supplement_fragment, container, false);

        SupplementContract supplementContract = new SupplementPresenter();
        supplementContract.getSupplementManager(getChildFragmentManager());
        supplementContract.getSupplementContext(getActivity());
        supplementContract.getSupplementView(view);
        supplementContract.setClickFinishBackStage(new SupplementContract.ClickFinishBackStage() {
            @Override
            public void finishBackStage() {
                getActivity().finish();
            }
        });

        return view;
    }

}
