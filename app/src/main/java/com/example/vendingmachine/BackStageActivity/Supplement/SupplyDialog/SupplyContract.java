package com.example.vendingmachine.BackStageActivity.Supplement.SupplyDialog;

import android.content.Context;
import android.view.View;

/**
 * Created by 冼栋梁 on 2018/4/10.
 */

public interface SupplyContract {

    void getSupplyContext(Context context);
    void getSupplyView(View view);

    void setClickCancleSupply(ClickCancleSupply clickCancleSupply);
    void setClickConfirmSupply(ClickConfirmSupply clickConfirmSupply);

    interface ClickCancleSupply{
        void finishSupply();
    }

    interface ClickConfirmSupply{
        void finishSupply();
    }
}
