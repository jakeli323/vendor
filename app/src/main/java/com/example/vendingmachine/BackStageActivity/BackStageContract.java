package com.example.vendingmachine.BackStageActivity;

import android.app.FragmentManager;
import android.widget.RadioGroup;

/**
 * Created by 冼栋梁 on 2018/4/9.
 */

public interface BackStageContract {

    void getNavigationGroup(RadioGroup backstage_navigationGroup);

    void getBackStageManager(FragmentManager backstage_fragmentManager);
}
