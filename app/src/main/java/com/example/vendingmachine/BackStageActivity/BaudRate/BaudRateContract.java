package com.example.vendingmachine.BackStageActivity.BaudRate;

import android.view.View;

public interface BaudRateContract {

    void getBaudRateView(View baudRateView);
}
