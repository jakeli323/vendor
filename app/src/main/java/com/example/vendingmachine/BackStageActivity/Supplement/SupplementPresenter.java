package com.example.vendingmachine.BackStageActivity.Supplement;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;

import com.example.vendingmachine.BackStageActivity.Supplement.SupplyDialog.SupplyFragment;
import com.example.vendingmachine.Data.Item;
import com.example.vendingmachine.Data.TestData;
import com.example.vendingmachine.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 冼栋梁 on 2018/4/9.
 */

public class SupplementPresenter implements SupplementContract,View.OnClickListener,AdapterView.OnItemClickListener {

    private FragmentManager supplementManager;
    private Context context;
    private ClickFinishBackStage clickFinishBackStage;

    public SupplementPresenter(){

    }

    @Override
    public void setClickFinishBackStage(ClickFinishBackStage clickFinishBackStage){

        this.clickFinishBackStage = clickFinishBackStage;
    }

    @Override
    public void getSupplementView(View view){

        ImageButton btn_backstage_supplement_finish_pressed = view.findViewById(R.id.img_btn_backstage_supplement_finish_pressed);
        btn_backstage_supplement_finish_pressed.setOnClickListener(this);

        ListView lv_backstage_supplement_display_click = view.findViewById(R.id.lv_backstage_supplement_display_click);
        lv_backstage_supplement_display_click.setOnItemClickListener(this);

        initListView(lv_backstage_supplement_display_click);

    }

    @Override
    public void getSupplementContext(Context context){

        this.context = context;
    }

    @Override
    public void getSupplementManager(FragmentManager supplementManager){

        this.supplementManager = supplementManager;
    }

    private void initListView(ListView listView){

        listView.setAdapter(new ListViewAdapter(context,initNameData()));

    }

    private List<String> initNameData(){

        List<String> nameList = new ArrayList<>();

        TestData testData = new TestData();
        List<Item> itemsList = testData.create_Item();

        final int size =itemsList.size();

        for (int i=0;i<size;i++){
            nameList.add(i,itemsList.get(i).getName());
        }

        return nameList;
    }

    @Override
    public void onClick(View view){

        switch (view.getId()){

            case R.id.img_btn_backstage_supplement_finish_pressed:clickFinishBackStage.finishBackStage();break;

        }

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id){

        final FragmentTransaction fragmentTransaction = supplementManager.beginTransaction();
        fragmentTransaction.add(R.id.rl_backstage_supplement_todialog_change,new SupplyFragment());
        fragmentTransaction.commit();

    }

}
