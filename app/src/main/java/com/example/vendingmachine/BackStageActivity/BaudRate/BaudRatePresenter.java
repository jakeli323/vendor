package com.example.vendingmachine.BackStageActivity.BaudRate;

import android.view.View;
import android.widget.Button;

import com.example.vendingmachine.R;
import com.example.vendingmachine.Util.SCMDataReceiveListener;
import com.example.vendingmachine.Util.SerialPortUtil;

public class BaudRatePresenter implements BaudRateContract,View.OnClickListener{

    public BaudRatePresenter(){

    }

    @Override
    public void getBaudRateView(View baudRateView){

        Button btn_backstage_baudrate_linkSCM_pressed = baudRateView.findViewById(R.id.btn_backstage_baudrate_linkSCM_pressed);
        btn_backstage_baudrate_linkSCM_pressed.setOnClickListener(this);
        Button btn_backstage_baudrate_querySCM_pressed = baudRateView.findViewById(R.id.btn_backstage_baudrate_querySCM_pressed);
        btn_backstage_baudrate_querySCM_pressed.setOnClickListener(this);

    }

    @Override
    public void onClick(View view){

        switch (view.getId()){

            case R.id.btn_backstage_baudrate_linkSCM_pressed:{

                SerialPortUtil.getInstance().setSCMDataReceiveListener(new SCMDataReceiveListener() {
                    @Override
                    public void dataRecevie(byte[] data, int size) {

                    }
                });

                byte[] bytes = {0x11};

                SerialPortUtil.getInstance().linkToSCM(bytes);

            }break;

            case R.id.btn_backstage_baudrate_querySCM_pressed:{

            }break;
        }
    }

}
