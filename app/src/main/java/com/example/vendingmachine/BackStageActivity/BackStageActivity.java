package com.example.vendingmachine.BackStageActivity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RadioGroup;

import com.example.vendingmachine.R;

public class BackStageActivity extends AppCompatActivity {

    private RadioGroup backstage_navigationGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.backstage_activity);

        backstage_navigationGroup = findViewById(R.id.radiogroup_backstage_navigation_pressed);

        BackStageContract backStageContract = new BackStagePresenter();
        backStageContract.getBackStageManager(getFragmentManager());
        backStageContract.getNavigationGroup(backstage_navigationGroup);

    }

}
