package com.example.vendingmachine.BackStageActivity.Supplement.SupplyDialog;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.vendingmachine.R;

/**
 * Created by 冼栋梁 on 2018/4/9.
 */

public class SupplyFragment extends DialogFragment {

    public SupplyFragment(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){

        View view = inflater.inflate(R.layout.backstage_supplement_supply_dialog, container, false);

        SupplyContract supplyContract = new SupplyPresenter();
        supplyContract.getSupplyContext(getActivity());
        supplyContract.getSupplyView(view);
        supplyContract.setClickCancleSupply(new SupplyContract.ClickCancleSupply() {
            @Override
            public void finishSupply() {
                dismiss();
            }
        });
        supplyContract.setClickConfirmSupply(new SupplyContract.ClickConfirmSupply() {
            @Override
            public void finishSupply() {
                dismiss();
            }
        });

        return view;
    }


}
