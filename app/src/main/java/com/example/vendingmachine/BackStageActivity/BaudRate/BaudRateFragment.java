package com.example.vendingmachine.BackStageActivity.BaudRate;


import android.os.Bundle;
import android.app.Fragment;
import android.preference.PreferenceFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.vendingmachine.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class BaudRateFragment extends PreferenceFragment {

    private BaudRateContract baudRateContract;

    public BaudRateFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.backstage_baudrate_fragment, container, false);

        baudRateContract = new BaudRatePresenter();
        baudRateContract.getBaudRateView(view);

        return view;
    }

}
