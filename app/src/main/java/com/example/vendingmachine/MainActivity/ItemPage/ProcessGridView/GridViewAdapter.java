package com.example.vendingmachine.MainActivity.ItemPage.ProcessGridView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.vendingmachine.Data.Item;
import com.example.vendingmachine.R;

import java.util.List;

/**
 * Created by 冼栋梁 on 2018/3/18.
 */

public class GridViewAdapter extends BaseAdapter {

    private List<Item> itemsList;

    private LayoutInflater inflater;

    private int currentIndex;

    private int pageSize;

    private Context context;

    public  GridViewAdapter(Context context, List<Item> itemsList, int currentIndex, int pageSize) {

        this.context = context;
        this.itemsList = itemsList;
        this.currentIndex = currentIndex;
        this.pageSize = pageSize;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {

        return itemsList.size() > (currentIndex + 1) * pageSize ? pageSize : (itemsList.size() - currentIndex * pageSize);

    }

    @Override
    public Object getItem(int position) {

        return itemsList.get(position + currentIndex * pageSize);

    }

    @Override
    public long getItemId(int position) {

        return position + currentIndex * pageSize;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;

        if (convertView == null) {

            convertView = inflater.inflate(R.layout.main_itempage_item_ll, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.item_iv_itemImage = convertView.findViewById(R.id.img_main_itempage_item_itemImage_desc);
            viewHolder.item_tv_itemPrice = convertView.findViewById(R.id.tv_main_itempage_item_itemPrice_desc);
            convertView.setTag(viewHolder);

        } else {

            viewHolder = (ViewHolder) convertView.getTag();

        }

        int pos = position + currentIndex * pageSize;

//        NumberFormat formatter = new DecimalFormat("#0.00");

        viewHolder.item_tv_itemPrice.setText(itemsList.get(pos).getPrice());

        Glide.with(context).load(itemsList.get(pos).getImagePath()).into(viewHolder.item_iv_itemImage);

        return convertView;
    }

    class ViewHolder {

        public TextView item_tv_itemPrice;
        public ImageView item_iv_itemImage;

    }

}
