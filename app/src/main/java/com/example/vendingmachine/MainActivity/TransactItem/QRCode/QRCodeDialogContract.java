package com.example.vendingmachine.MainActivity.TransactItem.QRCode;

import android.view.View;

public interface QRCodeDialogContract {

    void getQRCodeDialogView(View view);
    void setClickFinishTransaction(ClickFinishTransaction finishTransactionListener);
    void setCountDownFinishTransaction(CountDownFinishTransaction finishTransactionListener);

    interface ClickFinishTransaction{
        void finishTransaction();
    }

    interface CountDownFinishTransaction{
        void finishTransaction();
    }
}
