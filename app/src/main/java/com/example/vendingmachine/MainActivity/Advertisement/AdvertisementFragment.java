package com.example.vendingmachine.MainActivity.Advertisement;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.vendingmachine.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AdvertisementFragment extends Fragment {

    private AdvertisementContract advertisementContract = new AdvertisementPresenter();

    public AdvertisementFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.main_advertisement_fragment, container, false);

        advertisementContract.getView(view);

        advertisementContract.getContext(getActivity());

        advertisementContract.getImages();

        return view;
    }

    @Override
    public void onResume(){
        super.onResume();

        advertisementContract.startAutoScrollView();

    }

    @Override
    public void onPause(){
        super.onPause();

        advertisementContract.stopAutoScrollView();

    }

}
