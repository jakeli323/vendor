package com.example.vendingmachine.MainActivity.UsingHelp.LoginDialog;


import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.example.vendingmachine.BackStageActivity.BackStageActivity;
import com.example.vendingmachine.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends DialogFragment {

    public LoginFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.main_usinghelp_login_dialog, container, false);

        LoginContract loginContract = new LoginPresenter();

        loginContract.getLoginContext(getActivity());
        loginContract.getLoginView(view);
        loginContract.setClickFinishLogin(new LoginContract.ClickFinishLogin() {
            @Override
            public void finishLogin() {
                dismiss();
            }
        });
        loginContract.setClickSuccessLogin(new LoginContract.ClickSuccessLogin() {
            @Override
            public void startBackStage() {

                Intent intentToBackStage = new Intent(getActivity(), BackStageActivity.class);
                startActivityForResult(intentToBackStage,4217419);

                dismiss();
            }
        });

        return view;
    }

}
