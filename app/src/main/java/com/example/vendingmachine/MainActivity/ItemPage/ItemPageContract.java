package com.example.vendingmachine.MainActivity.ItemPage;


import android.content.Context;
import android.view.View;

import com.example.vendingmachine.Data.Item;

import java.util.List;

/**
 * Created by 冼栋梁 on 2018/3/13.
 */

public interface ItemPageContract {

    void getContext(Context context);
    void getView(View view);
    void getItems(List<Item> itemsList);
    void setPageCount();
    void initializationGridView();
    void setClickPassItem(ClickPassItem clickPassItem);

    interface ClickPassItem{

        void passItem(Item item);
    }

}
