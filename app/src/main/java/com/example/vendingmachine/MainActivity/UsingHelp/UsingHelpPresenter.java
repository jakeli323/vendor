package com.example.vendingmachine.MainActivity.UsingHelp;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;

import com.example.vendingmachine.MainActivity.UsingHelp.LoginDialog.LoginFragment;
import com.example.vendingmachine.R;

/**
 * Created by 冼栋梁 on 2018/4/10.
 */

public class UsingHelpPresenter implements UsingHelpContract,View.OnLongClickListener {

    private FragmentManager fragmentManager;

    public UsingHelpPresenter(){

    }

    @Override
    public void getFragmentManager(FragmentManager fragmentManager){

        this.fragmentManager = fragmentManager;
    }

    @Override
    public void getUsingHelpView(View view){

        ImageView img_main_usinghelp_setting_pressed = view.findViewById(R.id.img_main_usinghelp_setting_pressed);

        img_main_usinghelp_setting_pressed.setOnLongClickListener(this);

    }

    @Override
    public boolean onLongClick(View v){

        FragmentTransaction fragmentTransaction  = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.rl_main_usinghelp_change_desc,new LoginFragment());
        fragmentTransaction.commit();

        return true;
    }
}
