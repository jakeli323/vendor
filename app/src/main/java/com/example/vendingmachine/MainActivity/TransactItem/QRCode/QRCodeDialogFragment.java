package com.example.vendingmachine.MainActivity.TransactItem.QRCode;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.vendingmachine.R;

/**
 * Created by 冼栋梁 on 2018/4/7.
 */

public class QRCodeDialogFragment extends DialogFragment {

    public QRCodeDialogFragment(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.main_transactitem_code_dialog, container, false);

        QRCodeDialogContract codeDialogContract = new QRCodeDialogPresenter();

        codeDialogContract.getQRCodeDialogView(view);
        codeDialogContract.setClickFinishTransaction(new QRCodeDialogContract.ClickFinishTransaction() {
            @Override
            public void finishTransaction() {
                dismiss();
            }
        });

        codeDialogContract.setCountDownFinishTransaction(new QRCodeDialogContract.CountDownFinishTransaction() {
            @Override
            public void finishTransaction() {
                dismiss();
            }
        });

        return view;
    }

}
