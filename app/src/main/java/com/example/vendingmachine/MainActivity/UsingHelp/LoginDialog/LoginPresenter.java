package com.example.vendingmachine.MainActivity.UsingHelp.LoginDialog;

import android.content.Context;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.vendingmachine.Data.Account;
import com.example.vendingmachine.Util.InternetDataUtil;
import com.example.vendingmachine.R;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

/**
 * Created by 冼栋梁 on 2018/3/28.
 */

public class LoginPresenter implements LoginContract,View.OnClickListener {

    private Context loginContext;
    private ClickFinishLogin clickFinishLogin;
    private ClickSuccessLogin clickSuccessLogin;
    private EditText et_main_usinghelp_login_AccountPassword_input;
    private EditText et_main_usinghelp_login_AccountNumber_input;

    @Override
    public void setClickFinishLogin(ClickFinishLogin clickFinishLogin){
        this.clickFinishLogin = clickFinishLogin;
    }

    @Override
    public void setClickSuccessLogin(ClickSuccessLogin clickSuccessLogin){
        this.clickSuccessLogin = clickSuccessLogin;
    }

    @Override
    public void getLoginContext(Context loginContext){
        this.loginContext = loginContext;
    }

    @Override
    public void getLoginView(View view) {

        final Button btn_main_usinghelp_login_back = view.findViewById(R.id.btn_main_usinghelp_login_back_pressed);
        btn_main_usinghelp_login_back.setOnClickListener(this);

        final Button btn_main_usinghelp_login_confirm = view.findViewById(R.id.btn_main_usinghelp_login_confirm_presssed);
        btn_main_usinghelp_login_confirm.setOnClickListener(this);

        et_main_usinghelp_login_AccountNumber_input = view.findViewById(R.id.et_main_usinghelp_login_AccountNumber_input);
        setAccountNumberAttributes(et_main_usinghelp_login_AccountNumber_input);
        et_main_usinghelp_login_AccountPassword_input = view.findViewById(R.id.et_main_usinghelp_login_AccountPassword_input);
        setAccountPasswordAttributes(et_main_usinghelp_login_AccountPassword_input);

    }

    private void setAttributes(EditText editText){

        editText.setGravity(Gravity.CENTER);
        editText.setSingleLine(true);
    }

    private void setAccountNumberAttributes(EditText account_number){

        setAttributes(account_number);
        account_number.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
    }

    private void setAccountPasswordAttributes(EditText account_password){

        setAttributes(account_password);
        account_password.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
    }

    @Override
    public void onClick(View view){

        switch (view.getId()){

            case R.id.btn_main_usinghelp_login_back_pressed:clickFinishLogin.finishLogin();break;
            case R.id.btn_main_usinghelp_login_confirm_presssed:{

                //getEditTextInput(et_main_usinghelp_login_AccountNumber_input,et_main_usinghelp_login_AccountPassword_input);
                clickSuccessLogin.startBackStage();
            }break;

        }

    }

    private void getEditTextInput(EditText accountNumber,EditText accountPassword){

        final String accountNumber_Input = accountNumber.getText().toString();
        final String accountPassword_Input = accountPassword.getText().toString();

        setInputNotNull(accountNumber_Input,accountPassword_Input);
    }

    private void setInputNotNull(String numberInput,String passwordInput){

        if (numberInput.isEmpty() && passwordInput.isEmpty()){

            final String error = "管理员登录信息均不能为空";
            toast(error);

        }

        if (numberInput.isEmpty()){

            final String numberInputError = "管理员账号不能为空，请重新输入";
            toast(numberInputError);

        }else {

            if (passwordInput.isEmpty()){

                final String passwordInputError = "管理员密码不能为空，请重新输入";
                toast(passwordInputError);

            }else {

                try {

                    encodeAccountJSON(numberInput,passwordInput);

                }catch (IOException e) {

                    e.printStackTrace();
                }

            }

        }

    }

    private void toast(String errorMessage){

        final Toast toast = Toast.makeText(loginContext,errorMessage,Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER,0,0);
        toast.show();
    }

    private void encodeAccountJSON(String number_input,String password_input) throws IOException{

        final Account account = new Account(number_input,password_input);
        final ObjectMapper accountMapper = new ObjectMapper();
        final String accountJSON = accountMapper.writeValueAsString(account);

        InternetDataUtil.getInstance().postAccountJSON(accountJSON);
    }

}
