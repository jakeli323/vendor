package com.example.vendingmachine.MainActivity.TransactItem;

import android.app.FragmentManager;
import android.content.Context;
import android.view.View;

import com.example.vendingmachine.Data.Item;

public interface TransactItemContract {

    void getTransactItemContext(Context transactItemContext);
    void getTransactItemManager(FragmentManager transactItemManager);
    void getTransactItemView(View transactItemView);
    void updataData(Item item);
    void setClickPassHide(ClickPassHide clickPassHide);

    interface ClickPassHide{

        void passHide();
    }
}
