package com.example.vendingmachine.MainActivity.Advertisement;

import android.content.Context;
import android.view.View;

/**
 * Created by 冼栋梁 on 2018/3/13.
 */

public interface AdvertisementContract {

    /**
     * <p>详细的接口函数功能，请跳转到AdvertisementComply类中查看</p>
     */

    void getView(View view);

    void getContext(Context context);

    void getImages();

    void startAutoScrollView();

    void stopAutoScrollView();

}
