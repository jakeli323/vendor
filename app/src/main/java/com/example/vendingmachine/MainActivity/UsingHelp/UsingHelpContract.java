package com.example.vendingmachine.MainActivity.UsingHelp;

import android.app.FragmentManager;
import android.view.View;

/**
 * Created by 冼栋梁 on 2018/4/10.
 */

public interface UsingHelpContract {

    void getFragmentManager(FragmentManager fragmentManager);

    void getUsingHelpView(View view);
}
