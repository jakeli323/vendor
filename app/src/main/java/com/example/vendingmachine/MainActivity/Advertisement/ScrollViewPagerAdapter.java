package com.example.vendingmachine.MainActivity.Advertisement;

import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

/**
 * Created by 冼栋梁 on 2018/3/19.
 */

public class ScrollViewPagerAdapter extends PagerAdapter {

    private List<ImageView> ImageView_List;

    public ScrollViewPagerAdapter(List<ImageView> ImageView_List){

        this.ImageView_List = ImageView_List;

    }

    @Override
    public int getCount(){

        /**  返回显示页面的数目   */

        Log.d("TAG","getCount()");

        return ImageView_List.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        /** 当前显示的图片  */

        Log.d("TAG","instantiateItem()");

        container.addView(ImageView_List.get(position));

        return ImageView_List.get(position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        /** 销毁上一张图片 */

        Log.d("TAG","destroyItem()");

        container.removeView((View) object);

    }

    @Override
    public boolean isViewFromObject(View view, Object object){

        Log.d("TAG","isViewFromObject()");

        return (view == object);
    }


}
