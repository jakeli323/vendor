package com.example.vendingmachine.MainActivity.UsingHelp.LoginDialog;

import android.content.Context;
import android.view.View;

/**
 * Created by 冼栋梁 on 2018/3/28.
 */

public interface LoginContract {

    void getLoginContext(Context loginContext);

    void getLoginView(View loginView);

    void setClickFinishLogin(ClickFinishLogin clickFinishLogin);

    void setClickSuccessLogin(ClickSuccessLogin clickSuccessLogin);

    interface ClickFinishLogin{
        void finishLogin();
    }

    interface ClickSuccessLogin{
        void startBackStage();
    }

}
