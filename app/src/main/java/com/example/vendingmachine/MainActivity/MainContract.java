package com.example.vendingmachine.MainActivity;

import android.app.FragmentManager;
import android.widget.RadioGroup;

import com.example.vendingmachine.Data.Item;

/**
 * Created by 冼栋梁 on 2018/3/24.
 */

public interface MainContract {

    void getNavigationGroup(RadioGroup main_navigationGroup);
    void getFragmentManager(FragmentManager main_fragmentManager);
    void hideItemPage(Item item);
    void hideTransactItem();

}
