package com.example.vendingmachine.MainActivity;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.widget.RadioGroup;

import com.example.vendingmachine.Data.Item;
import com.example.vendingmachine.MainActivity.Activities.ActivitiesFragment;
import com.example.vendingmachine.MainActivity.ItemPage.ItemPageFragment;
import com.example.vendingmachine.MainActivity.TransactItem.TransactItemFragment;
import com.example.vendingmachine.MainActivity.UsingHelp.UsingHelpFragment;
import com.example.vendingmachine.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 冼栋梁 on 2018/3/24.
 */

public class MainPresenter implements MainContract,RadioGroup.OnCheckedChangeListener {

    private FragmentManager main_fragmentManager;
    private List<Fragment> fragmentList;
    private TransactItemFragment transactItemFragment;

    public MainPresenter(){

    }

    @Override
    public void getFragmentManager(FragmentManager fragmentManager){

        main_fragmentManager = fragmentManager;
    }

    @Override
    public void getNavigationGroup(RadioGroup main_navigationGroup){

        initNavigationBarFragments();
        initTransactItemFragment();
        main_navigationGroup.check(R.id.radio_btn_main_navigationbar_itempage_pressed);
        switchFragment(0);
        main_navigationGroup.setOnCheckedChangeListener(this);

    }

    @Override
    public void hideItemPage(Item item){

        transactItemFragment.getItem(item);
        FragmentTransaction fragmentTransaction  = main_fragmentManager.beginTransaction();
        fragmentTransaction.show(transactItemFragment);
        fragmentTransaction.hide(fragmentList.get(0));
        fragmentTransaction.commit();
    }

    @Override
    public void hideTransactItem(){

        FragmentTransaction fragmentTransaction = main_fragmentManager.beginTransaction();
        fragmentTransaction.hide(transactItemFragment);
        fragmentTransaction.show(fragmentList.get(0));
        fragmentTransaction.commit();

    }

    private void initNavigationBarFragments(){

        fragmentList = new ArrayList<>();

        ItemPageFragment itemPageFragment = new ItemPageFragment();
        fragmentList.add(0,itemPageFragment);
        UsingHelpFragment usingHelpFragment =  new UsingHelpFragment();
        fragmentList.add(1,usingHelpFragment);
        ActivitiesFragment activitiesFragment = new ActivitiesFragment();
        fragmentList.add(2,activitiesFragment);


    }

    private void initTransactItemFragment(){

        transactItemFragment = new TransactItemFragment();
        FragmentTransaction fragmentTransaction = main_fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.rl_main_transact_change,transactItemFragment);
        fragmentTransaction.hide(transactItemFragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId){

        switch(checkedId){

            case R.id.radio_btn_main_navigationbar_itempage_pressed:switchFragment(0);break;
            case R.id.radio_btn_main_navigation_usehelp_pressed:switchFragment(1);break;
            case R.id.radio_btn_main_navigation_activity_pressed:switchFragment(2);break;
        }

    }

    private void switchFragment(int index){

        final FragmentTransaction fragmentTransaction = main_fragmentManager.beginTransaction();

        final int size = fragmentList.size();

        for (int i = 0; i < size; i++) {

            Fragment fragment = fragmentList.get(i);

            if (i==index){

                if (fragment.isAdded()){

                    fragmentTransaction.show(fragment);

                }else{

                    fragmentTransaction.add(R.id.rl_main_changefragment_change,fragment);

                }

            }else{

                if (fragment.isAdded()){

                    fragmentTransaction.hide(fragment);

                }
            }
        }

        fragmentTransaction.commit();
    }

}
