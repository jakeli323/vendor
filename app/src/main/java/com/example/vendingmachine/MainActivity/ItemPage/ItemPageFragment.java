package com.example.vendingmachine.MainActivity.ItemPage;


import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.vendingmachine.Data.Item;
import com.example.vendingmachine.Util.ItemConvertListener;
import com.example.vendingmachine.Data.TestData;
import com.example.vendingmachine.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class ItemPageFragment extends Fragment {

    private ItemConvertListener itemConvertListener;

    public ItemPageFragment() {

    }

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);

        itemConvertListener = (ItemConvertListener)activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view=  inflater.inflate(R.layout.main_itempage_fragment, container, false);

        ItemPageContract itemPageContract = new ItemPagePresenter();
        TestData testData = new TestData();

        itemPageContract.getContext(getActivity());
        itemPageContract.getItems(testData.create_Item());
        itemPageContract.setPageCount();
        itemPageContract.getView(view);
        itemPageContract.initializationGridView();

        itemPageContract.setClickPassItem(new ItemPageContract.ClickPassItem() {
            @Override
            public void passItem(Item item) {

                itemConvertListener.convertItem(item);
            }
        });

        return view;
    }

}
