package com.example.vendingmachine.MainActivity;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RadioGroup;

import com.example.vendingmachine.Util.HideConvertListener;
import com.example.vendingmachine.Data.Item;
import com.example.vendingmachine.Util.ItemConvertListener;
import com.example.vendingmachine.R;
import com.squareup.leakcanary.LeakCanary;

public class MainActivity extends AppCompatActivity implements ItemConvertListener,HideConvertListener{

    private MainContract mainContract = new MainPresenter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        LeakCanary.install(getApplication());

        RadioGroup main_navigationGroup = findViewById(R.id.radiogroup_main_navigationbar_pressed);

        mainContract.getFragmentManager(getFragmentManager());
        mainContract.getNavigationGroup(main_navigationGroup);

    }

    @Override
    public void convertItem(Item item){

        mainContract.hideItemPage(item);
    }

    @Override
    public void convertHide(){

        mainContract.hideTransactItem();
    }

}
