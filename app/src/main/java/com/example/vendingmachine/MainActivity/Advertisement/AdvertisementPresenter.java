package com.example.vendingmachine.MainActivity.Advertisement;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.example.vendingmachine.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;

/**
 * Created by 冼栋梁 on 2018/3/13.
 */

public class AdvertisementPresenter implements AdvertisementContract {

    private AutoScrollViewPager autoScrollViewPager;
    private Context context;

    public AdvertisementPresenter(){

    }

    @Override
    public void getView(View view){

        autoScrollViewPager = view.findViewById(R.id.advertisement_view_pager_autoScrollImage);

        setAutoScrollViewPagerParameter(autoScrollViewPager);

    }

    @Override
    public void getContext(Context context){

        this.context = context;

    }

    private void setAutoScrollViewPagerParameter(AutoScrollViewPager autoScrollViewPager){

        autoScrollViewPager.setInterval(3000);
        autoScrollViewPager.setDirection(AutoScrollViewPager.RIGHT);
        autoScrollViewPager.setCycle(true);
        autoScrollViewPager.setScrollDurationFactor(20);
        autoScrollViewPager.setSlideBorderMode(AutoScrollViewPager.SLIDE_BORDER_MODE_CYCLE);
        autoScrollViewPager.setBorderAnimation(false);

    }

    @Override
    public void startAutoScrollView(){

        autoScrollViewPager.startAutoScroll();

    }

    @Override
    public void stopAutoScrollView(){

        autoScrollViewPager.stopAutoScroll();

    }

    @Override
    public void getImages(){

        List<String> URL_List = new ArrayList<>();

        URL_List.add("https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=1085607972,1978119061&fm=27&gp=0.jpg");
        URL_List.add("https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=2376347538,3176445493&fm=27&gp=0.jpg");
        URL_List.add("https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=188345605,583367870&fm=27&gp=0.jpg");

        picassoImage(context,URL_List);

    }

    /**
     * functionName:picassoImage
     *
     * function:which use Picasso (which use to get the Image from URL) to DownLoad image from URL and need't use Bitmap
     * */
    private void picassoImage(Context context,List<String> URL_List){

        List<ImageView> ImageView_List = new ArrayList<>();

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        int length = URL_List.size();

        for(int i = 0 ; i < length ; i++ ){

            ImageView imageView = (ImageView) inflater.inflate(R.layout.main_advertisement_item,null);

            Picasso.get().load(URL_List.get(i)).into(imageView);

            ImageView_List.add(i,imageView);

        }

        setAdapter(ImageView_List);

    }

    private void setAdapter(List<ImageView> imageView_List){

        autoScrollViewPager.setAdapter(new ScrollViewPagerAdapter(imageView_List));

    }

}
