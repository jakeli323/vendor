package com.example.vendingmachine.MainActivity.TransactItem;


import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.vendingmachine.Util.HideConvertListener;
import com.example.vendingmachine.Data.Item;
import com.example.vendingmachine.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class TransactItemFragment extends Fragment {

    private TransactItemContract transactItemContract = new TransactItemPresenter();
    private HideConvertListener hideConvertListener;

    public TransactItemFragment() {

    }

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);

        hideConvertListener = (HideConvertListener) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.main_transactitem_fragment, container, false);

        transactItemContract.getTransactItemContext(getActivity());
        transactItemContract.getTransactItemManager(getChildFragmentManager());
        transactItemContract.getTransactItemView(view);
        transactItemContract.setClickPassHide(new TransactItemContract.ClickPassHide() {
            @Override
            public void passHide() {
                hideConvertListener.convertHide();
            }
        });

        return view;
    }

    public void getItem(Item item){

        transactItemContract.updataData(item);
    }

}
