package com.example.vendingmachine.MainActivity.ItemPage.ProcessGridView;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by 冼栋梁 on 2018/3/18.
 */

public class GridViewPagerAdapter extends PagerAdapter {

    private List<View> viewsList;

    public GridViewPagerAdapter(List<View> viewsList) {

        this.viewsList = viewsList;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        container.removeView(viewsList.get(position));

    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        container.addView(viewsList.get(position));

        return (viewsList.get(position));
    }

    @Override
    public int getCount() {

        if (viewsList == null){

            return 0;

        }

        return viewsList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

}
