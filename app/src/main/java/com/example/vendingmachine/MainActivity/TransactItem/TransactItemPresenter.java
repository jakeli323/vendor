package com.example.vendingmachine.MainActivity.TransactItem;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.vendingmachine.Data.Item;
import com.example.vendingmachine.MainActivity.TransactItem.QRCode.QRCodeDialogFragment;
import com.example.vendingmachine.R;
import com.squareup.picasso.Picasso;

public class TransactItemPresenter implements TransactItemContract,View.OnClickListener{

    private Context TransactItemContext;
    private TextView tv_main_itempage_transaction_buynumber_change;
    private TextView tv_main_itempage_transaction_name_desc;
    private TextView tv_main_itempage_transaction_price_change;
    private ImageView img_main_itempage_transaction_image_desc;
    private ClickPassHide clickPassHide;
    private FragmentManager transactItemManager;

    private int itemPurchase = 1;

    public TransactItemPresenter(){

    }

    public void setClickPassHide(ClickPassHide clickPassHide){

        this.clickPassHide = clickPassHide;
    }

    @Override
    public void getTransactItemView(View transactItemView){

        Button btn_main_itempage_transaction_back_pressed = transactItemView.
                findViewById(R.id.btn_main_itempage_transaction_back_pressed);
        btn_main_itempage_transaction_back_pressed.setOnClickListener(this);

        Button btn_main_itempage_transaction_reduceitem_pressed = transactItemView.
                findViewById(R.id.btn_main_itempage_transaction_reduceitem_pressed);
        btn_main_itempage_transaction_reduceitem_pressed.setOnClickListener(this);

        Button btn_main_itempage_transaction_increaseditem_pressed = transactItemView.
                findViewById(R.id.btn_main_itempage_transaction_increaseditem_pressed);
        btn_main_itempage_transaction_increaseditem_pressed.setOnClickListener(this);

        Button btn_main_itempage_transaction_confirm_pressed = transactItemView.
                findViewById(R.id.btn_main_itempage_transaction_confirm_pressed);
        btn_main_itempage_transaction_confirm_pressed.setOnClickListener(this);

        tv_main_itempage_transaction_buynumber_change = transactItemView.
                findViewById(R.id.tv_main_itempage_transaction_buynumber_change);

        tv_main_itempage_transaction_name_desc = transactItemView.
                findViewById(R.id.tv_main_itempage_transaction_name_desc);

        tv_main_itempage_transaction_price_change = transactItemView.
                findViewById(R.id.tv_main_itempage_transaction_price_change);

        img_main_itempage_transaction_image_desc = transactItemView.
                findViewById(R.id.img_main_itempage_transaction_image_desc);

    }

    @Override
    public void getTransactItemContext(Context TransactItemContext){

        this.TransactItemContext = TransactItemContext;
    }

    @Override
    public void getTransactItemManager(FragmentManager trasactItemManager){

        this.transactItemManager = trasactItemManager;
    }

    @Override
    public void onClick(View v){

        switch (v.getId()){

            case R.id.btn_main_itempage_transaction_back_pressed:{

                resetBuyNumber();
                clickPassHide.passHide();
            }break;

            case R.id.btn_main_itempage_transaction_confirm_pressed:{

                final FragmentTransaction fragmentTransaction = transactItemManager.beginTransaction();
                fragmentTransaction.add(R.id.rl_main_transactitem_change,new QRCodeDialogFragment());
                fragmentTransaction.commit();

            }break;

            case R.id.btn_main_itempage_transaction_reduceitem_pressed:{

                itemPurchase = (itemPurchase - 1);

                if (itemPurchase <= 0){

                    resetBuyNumber();

                }

                tv_main_itempage_transaction_buynumber_change.setText(String.valueOf(itemPurchase));

            }break;

            case R.id.btn_main_itempage_transaction_increaseditem_pressed:{

                itemPurchase = (itemPurchase + 1);
                tv_main_itempage_transaction_buynumber_change.setText(String.valueOf(itemPurchase));

            }break;

        }

    }

    @Override
    public void updataData(Item item){

        tv_main_itempage_transaction_buynumber_change.setText(String.valueOf(1));
        tv_main_itempage_transaction_name_desc.setText(item.getName());
        tv_main_itempage_transaction_price_change.setText(item.getPrice());
        Glide.with(TransactItemContext).load(item.getImagePath()).into(img_main_itempage_transaction_image_desc);

    }

    private void resetBuyNumber(){

        itemPurchase = 1;
    }

}
