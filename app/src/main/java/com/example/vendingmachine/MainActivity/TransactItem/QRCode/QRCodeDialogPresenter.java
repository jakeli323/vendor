package com.example.vendingmachine.MainActivity.TransactItem.QRCode;

import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vendingmachine.R;
import com.example.vendingmachine.Util.QRCodeUtil;

public class QRCodeDialogPresenter implements QRCodeDialogContract,View.OnClickListener{

    private ClickFinishTransaction clickFinishListener;
    private CountDownFinishTransaction countDownFinishListener;

    public QRCodeDialogPresenter(){

    }

    @Override
    public void getQRCodeDialogView(View view){

        Button btn_codeDialog_back_pressed = view.findViewById(R.id.btn_codeDialog_back_pressed);
        btn_codeDialog_back_pressed.setOnClickListener(this);

        initCountDown(view);
    }

    @Override
    public void setClickFinishTransaction(ClickFinishTransaction clickFinishListener){

        this.clickFinishListener = clickFinishListener;
    }

    @Override
    public void setCountDownFinishTransaction(CountDownFinishTransaction countDownFinishListener){

        this.countDownFinishListener = countDownFinishListener;
    }

    private void initCountDown(View view){

        final TextView tv_codeDialog_show = view.findViewById(R.id.tv_codeDialog_countDown_show);
        final ImageView img_codeDialog_show = view.findViewById(R.id.img_codeDialog_QRCode_show);
        img_codeDialog_show.setImageBitmap(QRCodeUtil.getInstance().encodeTransactionString("10086000",100));

        new CountDownTimer(60000,1000){

            public void onTick(long millisUntilFinished) {

                long timeLeft = millisUntilFinished/1000;

                StringBuilder timeLeftWarming = new StringBuilder("交易关闭还剩");
                timeLeftWarming.append(String.valueOf(timeLeft));
                timeLeftWarming.append("秒");

                tv_codeDialog_show.setText(timeLeftWarming.toString());

            }

            public void onFinish() {

                countDownFinishListener.finishTransaction();

            }

        }.start();

    }

    @Override
    public void onClick(View view){

        switch (view.getId()){
            case R.id.btn_codeDialog_back_pressed:{

                clickFinishListener.finishTransaction();
            }
        }
    }
}
