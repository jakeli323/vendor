package com.example.vendingmachine.MainActivity.UsingHelp;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.vendingmachine.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class UsingHelpFragment extends Fragment {

    public UsingHelpFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_usinghelp_fragment, container, false);

        UsingHelpContract usingHelpContract = new UsingHelpPresenter();

        usingHelpContract.getFragmentManager(getChildFragmentManager());
        usingHelpContract.getUsingHelpView(view);

        return view;
    }

}
