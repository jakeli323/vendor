package com.example.vendingmachine.MainActivity.ItemPage;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;

import com.example.vendingmachine.Data.Item;
import com.example.vendingmachine.MainActivity.ItemPage.ProcessGridView.GridViewAdapter;
import com.example.vendingmachine.MainActivity.ItemPage.ProcessGridView.GridViewPagerAdapter;
import com.example.vendingmachine.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 冼栋梁 on 2018/3/13.
 */

public class ItemPagePresenter implements ItemPageContract {

    private int pageCount;
    private final int pageSize = 10;
    private int currentIndex;
    private ViewPager viewPager;
    private LayoutInflater inflater;
    private Context context;
    private List<Item> itemsList;
    private LinearLayout indicator;
    private ClickPassItem clickPassItem;

    public ItemPagePresenter(){

    }

    @Override
    public void getContext(Context context){

        this.context = context;
    }

    @Override
    public void getView(View view){

        viewPager = view.findViewById(R.id.viewpager_main_itempage_display_desc);
        indicator = view.findViewById(R.id.ll_main_itempage_indicator_desc);

    }

    @Override
    public void getItems(List<Item> itemsList){

        this.itemsList = itemsList;

    }

    @Override
    public void initializationGridView(){

        List<View> viewsList = new ArrayList<>();

        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        for(int i = 0 ; i < pageCount ; i++){

            GridView gridView = (GridView) inflater.inflate(R.layout.main_itempage_gridview,viewPager,false);
            gridView.setAdapter(new GridViewAdapter(context,itemsList,i,pageSize));
            viewsList.add(gridView);
            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener(){

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    int pos =(position + currentIndex * pageSize);
                    Item item = itemsList.get(pos);
                    clickPassItem.passItem(item);
                }
            });

        }

        viewPager.setAdapter(new GridViewPagerAdapter(viewsList));
        setIndicator();

    }

    @Override
    public void setClickPassItem(ClickPassItem clickPassItem){

        this.clickPassItem = clickPassItem;
    }

    @Override
    public void setPageCount(){

        pageCount = (int) Math.ceil(itemsList.size()*1.0/pageSize);

    }

    private void setIndicator() {

        for (int i = 0; i < pageCount; i++) {

            indicator.addView(inflater.inflate(R.layout.main_itempage_indicator, null));

        }

        indicator.getChildAt(0).findViewById(R.id.itempage_indicator_item).setBackgroundResource(R.drawable.main_itempage_indicator_select_border);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            public void onPageSelected(int position) {

                indicator.getChildAt(currentIndex).findViewById(R.id.itempage_indicator_item)
                        .setBackgroundResource(R.drawable.main_itempage_indicator_unselect_border);
                indicator.getChildAt(position).findViewById(R.id.itempage_indicator_item)
                        .setBackgroundResource(R.drawable.main_itempage_indicator_select_border);
                currentIndex = position;

            }

            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            public void onPageScrollStateChanged(int arg0) {
            }

        });

    }

}


