package com.example.vendingmachine.Util;

import com.example.vendingmachine.Data.Item;

public interface ItemConvertListener {

    void convertItem(Item item);
}
