package com.example.vendingmachine.Util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android_serialport_api.SerialPort;

public class SerialPortUtil {

    private static SerialPortUtil INSTANCE = null;
    private SerialPort serialPort;
    private OutputStream outputStream;
    private InputStream inputStream;
    private SCMDataReceiveListener SCMDataReceiveListener;

    private SerialPortUtil(){

    }

    public static SerialPortUtil getInstance(){

        if (INSTANCE == null){
            INSTANCE = new SerialPortUtil();
        }

        return INSTANCE;
    }

    public void setSCMDataReceiveListener(SCMDataReceiveListener SCMDataReceiveListener) {

        this.SCMDataReceiveListener = SCMDataReceiveListener;
    }

    private void openSerialPort(){

        try {

            final String portPath = "/dev/ttyS0";
            final int baudRate = 115200;

            serialPort = new SerialPort(new File(portPath),baudRate,0);
            outputStream = serialPort.getOutputStream();
            inputStream = serialPort.getInputStream();

            new ReadThread().start();

        }catch (IOException e){
            e.printStackTrace();
        }

    }

    public void closeSerialPort(){

        try{

            inputStream.close();
            outputStream.close();
            serialPort.close();

        }catch (IOException e){

            e.printStackTrace();
        }
    }

    private void sendDataToSerialPort(byte[] data){

        try{

            int dataLength = data.length;

            if(dataLength > 0){

                outputStream.write(data);
                outputStream.write('\n');
                outputStream.flush();
            }

        }catch (IOException e){

            e.printStackTrace();
        }
    }

    private class ReadThread extends Thread {

        @Override
        public void run() {
            super.run();
            while(!isInterrupted()) {

                int size;

                try {

                    byte[] buffer = new byte[1];

                    if (inputStream == null) {
                        return;
                    }

                    size = inputStream.read(buffer);

                    if (size > 0) {
                        SCMDataReceiveListener.dataRecevie(buffer, size);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    return;
                }

            }
        }
    }

    //测试与单片机的链接
    public void linkToSCM(byte[] link){

        final String CMD_SLV_LINK = "CMD_SLV_LINK";
        openSerialPort();
        sendDataToSerialPort(link);
    }

    //查询单片机的状态
    public void quarySCMStatus(byte[] query){

        final String CMD_PC_POLL = "CMD_PC_POLL";
    }

    //从单片机中获取货道
    public void obtainCargoWayFromSCM(){

        final String CMD_PC_SLOT_TOPC = "CMD_PC_SLOT_TOPC";

    }

    //发送货道信息给单片机
    public void sendCargoWayToSCM(){

        final String CMD_PC_SLOT_TOTM = "CMD_PC_SLOT_TOTM";
    }

    //发送货物给客户
    public void sellItemToCustom(){

        final String CMD_PC_TRANSFOR = "CMD_PC_TRANSFOR";
    }

    //发送交易信息给单片机
    public void sendTransactionToSCM(){

        final String CMD_PC_SEND_TRADE = "CMD_PC_SEND_TRADE";
    }

    //同步客户端与单片机的时间
    public void syncTimeWithSCM(){

        final String CMD_PC_SYNC_TIME = "CMD_PC_SYNC_TIME";
    }

    //清除所有的进货信息
    public void deleteReloadMessageInSCM(){

        final String CMD_PC_RELOAD_OK = "CMD_PC_RELOAD_OK";
    }

    //掉电把所有的货道数据存储在存储器中
    public void sendInstructMemoryCargo(){

        final String CMD_PC_WRT_SLOT = "CMD_PC_WRT_SLOT";
    }

}
