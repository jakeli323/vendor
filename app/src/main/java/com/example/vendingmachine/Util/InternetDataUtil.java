package com.example.vendingmachine.Util;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class InternetDataUtil {

    private InternetDataUtil(){


    }

    private static InternetDataUtil INSTANCE = null;
    private static OkHttpClient okHttpClient = null;

    public static InternetDataUtil getInstance(){

        if (INSTANCE == null){

            INSTANCE = new InternetDataUtil();

            if (okHttpClient == null){

                okHttpClient = new OkHttpClient();
            }
        }

        return INSTANCE;
    }

    private String serverAddress(){

        final String PROTOCOL = "http";
        final String IP = "192.168.1.100";
        final String PORT = "9988";

        final StringBuilder URLBuilder = new StringBuilder();
        final String URL = URLBuilder
                .append(PROTOCOL).append("://")
                .append(IP).append(":")
                .append(PORT).append("/").toString();

        return URL;
    }

    public void postAccountJSON(String accountJSON){

        final String accountURL = serverAddress();
        final MediaType JSONType = MediaType.parse("application/json; charset=utf-8");
        final RequestBody body = RequestBody.create(JSONType,accountJSON);
        final Request request = new Request.Builder().url(accountURL).post(body).build();
        final Call call = okHttpClient.newCall(request);

        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

            }
        });

    }

    public void postNumberJSON(String numberJSON){

        final String numberURL = serverAddress();
        final MediaType JSONType = MediaType.parse("application/json; charset=utf-8");
        final RequestBody body = RequestBody.create(JSONType,numberJSON);
        final Request request = new Request.Builder().url(numberURL).post(body).build();
        final Call call = okHttpClient.newCall(request);

        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

            }
        });

    }

    public void getAdvertisementURL(){


    }

}
